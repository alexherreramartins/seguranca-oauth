package br.com.mastertech.seguranca;

import br.com.mastertech.seguranca.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seguranca")
public class SegurancaController {

    @GetMapping
    public Usuario getSeguranca( @AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
